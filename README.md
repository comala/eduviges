# Eduviges
> –¿Qué es lo que hay aquí? –pregunté.
>
> –Tiliches –me dijo ella–. Tengo la casa toda entilichada.

## Purpose
This package intends to construct light curves and spectra out of simulations. Also calculate the photon flux, photon spectral index and fit spectral energy distributions (SEDs).

## Requirements

- numpy
- scipy
- h5py

## How to
- Clone repository

### Example
The file [`plots.py`](https://bitbucket.org/comala/workspace/snippets/LpLgGL#file-plots.py) in the [snippets](https://bitbucket.org/comala/workspace/snippets/) is a `Python` script that plots the light curves, SEDs and particles distribution functions from the afterglow simulation with the parameters of GRB190114C.

## Description of the modules

### `magnetobrem`

In this module the following physical constants are defined: `cLight`, `eCharge`, `hPlanck`, `me`, `mp`, `sigmaT`, `nuconstn = e / (2 pi m_e c)`, `halfpi = pi / 2`, `twopi = 2 pi`.


### `pwlFuncs`

### `spectra`

### `SRtoolkit`

### `fits.phFlux`

### `fits.SEDfit`

## TODO

- [ ] Documentation.
- [ ] Update tests in tests folder
  - Those are scripts meant to test the routines and functions but haven't been updated to the current state of the repository.
- [ ] Compare + merge `SEDfit.py`
  - [ ] Implement new fitting methods?
- [ ] Test models.
  - **NOTE**: I think I tested them but don't recall. This could be done with the afterglow routine in `Paramo`.
- [ ] Test `Renteria.py`
  - `Renteria.py` has just been merged from another repository ([https://github.com/altjerue/extractor](extractor)), and converted into a module with two classes. 
