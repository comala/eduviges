import sys
import numpy as np
import scipy.interpolate
import h5py as h5


class fromASCII(object):
    '''This class contains macros to extract data from ASCII
    '''

    def __init__(self, file=None):
        if isinstance(file, type('a')):
            sys.exit("No input file")
        else:
            self.fname = file

    def interpol(self, v1, v2, tt, NxM, logscale=False, rescale=False):
        """Interpolate data with not a regular grid."""
        N = NxM[0]
        M = NxM[1]
        if logscale is True:
            v1i = np.logspace(np.log10(v1.min()), np.log10(v1.max()), N)
            v2i = np.logspace(np.log10(v2.min()), np.log10(v2.max()), M)
        else:
            v1i = np.linspace(v1.min(), v1.max(), N)
            v2i = np.linspace(v2.min(), v2.max(), M)
        ti = scipy.interpolate.griddata((v1, v2), tt,
                                        (v1i[None, :], v2i[:, None]),
                                        method='cubic', rescale=rescale)
        return v1i, v2i, ti

    def dataExtract3col(self, cols=(0, 1, 2), rescale=False):
        """Extract 3 columns from a data file.

        Three columns extraction routine. Getting data ready for contour
        plotting.

        filename: string
        Name of the file with at least four columns.

        cols: Tuple
        Tuple of three int referring to the columns to be read.
        """
        return np.loadtxt(self.fname, usecols=cols, unpack=True)

    def dataExtract2col(self, cols=(0, 1)):
        """Extract 3 columns from a data file.

        Four columns extraction routine. Getting data ready for contour
        plotting.

        filename: string
        Name of the file with at least four columns.

        cols: tuple
        Tuple of two int referring to the columns to be read.
        """
        return np.loadtxt(self.fname, usecols=cols, unpack=True)


class fromHDF5(object):
    '''This class contains macros to extract data from HDF5
    '''

    h5fname = None

    def __init__(self, h5fname):
        self.h5file = h5fname

    def hdf5ExtractScalar(self, dsets, group=None):
        """Extract data from an HDF5 data file.

        h5file: string
        File name.

        dsets: strings
        scalar datasets names
        """

        h5f = h5.File(self.h5file, 'r')
        if group is None:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[dsets[i]][0])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[dsets][0]
        else:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[group][dsets[i]][0])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[group][dsets][0]
        h5f.close()
        return v

    def hdf5Extract1D(self, dsets, group=None):
        """Extract data from an HDF5 data file.

        h5file: string
        File name.

        ds1,ds2: strings
        1D data set names
        """
        h5f = h5.File(self.h5file, 'r')
        if group is None:
            if type(dsets) is list:
                v = []
                for dset in dsets:
                    v.append(h5f[dset][:])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[dsets][:]
        else:
            if type(dsets) is list:
                v = []
                for dset in dsets:
                    v.append(h5f[group][dsets][:])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[group][dsets][:]
        h5f.close()
        return v

    def hdf5Extract2D(self, dsets, group=None):
        """Extract data from an HDF5 data file.

        h5file: string
        File name.

        dsets: string or array of strings
        2D data set names
        """
        h5f = h5.File(self.h5file, 'r')
        if group is None:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[dsets[i]][:, :])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[dsets][:, :]
        else:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[group][dsets[i]][:, :])
                v = np.array(v)
                v = v.squeeze()
            else:
                v = h5f[group][dsets][:, :]
        h5f.close()
        return v

    def hdf5Extract3D(self, dsets, group=None):
        """Extract data from an HDF5 data file.

        h5file: string
        File name.

        dsets: strings or array of strings
        3D data set names
        """
        h5f = h5.File(self.h5file, 'r')
        if group is None:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[dsets[i]][:, :, :])
            else:
                v = h5f[dsets][:, :, :]
        else:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[group][dsets[i]][:, :, :])
            else:
                v = h5f[group][dsets][:, :, :]
        h5f.close()
        return v

    def hdf5Extract4D(self, dsets, group=None):
        """Extract data from an HDF5 data file.

        h5file: string
        File name.

        dsets: strings or array of strings
        3D data set names
        """
        h5f = h5.File(self.h5file, 'r')
        if group is None:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[dsets[i]][:, :, :, :])
            else:
                v = h5f[dsets][:, :, :, :]
        else:
            if type(dsets) is list:
                v = []
                for i in range(0, len(dsets)):
                    v.append(h5f[group][dsets[i]][:, :, :, :])
            else:
                v = h5f[group][dsets][:, :, :, :]
        h5f.close()
        return v
