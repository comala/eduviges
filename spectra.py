import numpy as np
import numpy.ma as ma
import scipy.integrate as sci_integ
from . import misc
from . import pwlFuncs as pwlf
from . import SRtoolkit as SR
from . import constants as C
from . import RadTrans as RT
import scipy.optimize as op
import scipy.interpolate as interpol


def erg2Jy(flux):
    '''Convert flux density (in egs cm^{-2} s^{-1} Hz^{-1}) to janskys.
    '''
    return flux * 1e23


def Jy2erg(flux):
    '''Convert flux density in jansky to egs cm^{-2} s^{-1} Hz^{-1}.
    '''
    return flux * 1e-23


def Hz2eV(nu):
    '''Convert frequency in hertz to energy in electronvolt
    '''
    return nu * 4.135667662e-15


def eV2Hz(nu):
    '''Convert energy in electronvolt to frequency in hertz
    '''
    return nu * 2.4179937422321953e14


def Hz2m(nu):
    '''Convert frequency in hertz to wavelength in meters
    '''
    return C.cLight * 1e-2 / nu


def m2Hz(wavelength):
    '''Convert frequency in hertz to wavelength in meters
    '''
    return C.cLight * 1e-2 / wavelength


def sec2dy(time):
    '''Convert time in seconds to days
    '''
    return time / 8.64e4


def dy2sec(time):
    '''Convert time in days to seconds
    '''
    return time * 86400.0


def sec2hr(time):
    '''Convert time in seconds to hours
    '''
    return time / 3600.0


def hr2sec(time):
    '''Convert time in hours to seconds
    '''
    return time * 3600.0


def pc2cm(distance):
    '''Convert distance from parsecs to centimeters
    '''
    return distance * 3.08567758149137e18


def cm2pc(distance):
    '''Convert distance from centimeters to parsecs
    '''
    return distance / 3.08567758149137e18


#    Energy Flux
class Flux(object):
    '''Energy flux class
    '''

    def params(self):
        self.z = 0.
        self.dL = 1e28
        self.mu_obs = 1.0
        self.G = 100.
        self.r = 1e16

    def __init__(self, **kwargs):
        self.params()
        self.__dict__.update(kwargs)
        self.D = SR.Doppler(self.G, self.mu_obs)

    def specEnerFlux(self, nu, jnu, r):
        '''Calculates the spectral energy flux of a sphere.
        '''
        if r.shape == ():
            volume = 4. * np.pi * r**3 / 3.
            Fnu = self.D**3 * (1 + self.z) * volume * jnu / (4. * np.pi * self.dL**2)
        else:
            Fnu = np.zeros_like(jnu)
            for i in range(self.D.size):
                for j in range(nu.size):
                    Fnu[i, j] = (1. + self.z) * sci_integ.trapz(jnu[:i+1, j] * (r[:i+1] * self.D[:i+1])**2, x=r[:i+1]) / (4. * np.pi * self.dL**2)
        return Fnu

    def EnerFlux(self, nu, jnu, r):
        '''Calculates the energy flux of a sphere.
        '''
        if r.shape == ():
            volume = 4. * np.pi * r**3 / 3.
            nuFnu = self.D**4 * nu * volume * jnu / (4. * np.pi * self.dL**2)
        else:
            nuFnu = np.zeros_like(jnu)
            for i in range(self.D.size):
                for j in range(nu.size):
                    nuFnu[i, j] = sci_integ.trapz(nu[j] * jnu[:i+1, j] * r[:i+1]**2 * self.D[:i+1]**3, x=r[:i+1]) / (4. * np.pi * self.dL**2)
        return nuFnu

    def LightCurve(self, nuo1, nuo2, nuc, jnu, specific=True, tol=1e-6):
        '''Light Curves
        '''
        licur = np.zeros(jnu.shape[0])

        if specific:
            flux = self.specEnerFlux(nuc, jnu, self.r)
        else:
            flux = self.EnerFlux(nuc, jnu, self.r)

        for i in range(jnu.shape[0]):
            nuo_tmp = SR.nu_obs(nuc, self.z, self.G[i], self.mu_obs)
            iFnu = interpol.interp1d(nuo_tmp, flux[i, :] / nuo_tmp, kind='cubic', fill_value="extrapolate")
            if nuo1 == nuo2:
                licur[i] = iFnu(nuo1) * nuo1
            else:
                licur[i] = sci_integ.romberg(iFnu, nuo1, nuo2, tol=tol)
        return licur

    #
    #
    #
    def AverSpec(self, to1, to2, to, nuc, jnu, nuo, specific=True, tol=1e-6):
        '''Averaged Spectrum
        '''
        spec = np.zeros_like(nuo)

        if specific:
            flux = self.specEnerFlux(nuc, jnu, self.r)
        else:
            flux = self.EnerFlux(nuc, jnu, self.r)

        ftmp = np.zeros((jnu.shape[0], nuo.size))
        for i in range(jnu.shape[0]):
            nuo_tmp = SR.nu_obs(nuc, self.z, self.G[i], self.mu_obs)
            for j in range(nuo.size):
                ftmp[i, j] = np.interp(nuo[j], nuo_tmp, flux[i, :])

        for j in range(nuo.size):
            iFnu = interpol.interp1d(to, ftmp[:, j], kind='cubic', fill_value="extrapolate")
            spec[j] = sci_integ.romberg(iFnu, to1, to2, tol=tol)

        return spec / (to2 - to1)


# class EATS(object):
#     '''Energy flux class
#     '''

#     def params(self):
#         self.z = 0.
#         self.dL = 1e28

#     def __init__(self, **kwargs):
#         self.params()
#         self.__dict__.update(kwargs)

#     def specEnerFlux_EATS(self, nu, jnu, r, ):
#         '''Calculates the spectral energy flux from an equal arrival time surface (EATS)
#         '''
#         Fnu = np.zeros_like(jnu)
#         Inu =
#         for i in range(self.D.size):
#             for j in range(nu.size):
#                 Fnu[i, j] = (1. + self.z) * sci_integ.trapz(jnu[:i+1, j] * (r[:i+1] * self.D[:i+1])**2, x=r[:i+1]) / (4. * np.pi * self.dL**2)
#         return Fnu


################################################################################
#   Luminosity
#
def specLuminosity(nu, jnu, anu, Doppler, radius, volume):
    '''Calculates the spectral energy flux of a sphere.
    '''
    if nu.shape == ():
        Lnu = Doppler**3 * volume * jnu * RT.OptDepthBlob_s(anu, radius)
    else:
        if anu.shape == (anu.size,):
            Lnu = Doppler**3 * volume * jnu * RT.OptDepthBlob_v(anu, radius)
        else:
            Lnu = Doppler**3 * volume * jnu * RT.OptDepthBlob_m(anu, radius)
    return Lnu


def Luminosity(nu, jnu, anu, Doppler, radius, volume):
    '''Calculates the energy flux of a sphere.
    '''
    if nu.shape == ():
        nuL_nu = Doppler**4 * volume * nu * jnu * RT.OptDepthBlob_s(anu, radius)
    else:
        if anu.shape == (anu.size,):
            nuL_nu = Doppler**4 * volume * nu * jnu * RT.OptDepthBlob_v(anu, radius)
        else:
            nuL_nu = Doppler**4 * volume * nu * jnu * RT.OptDepthBlob_m(anu, radius)
    return nuL_nu
################################################################################


# ----->   Bolometric function
def lumBolometric(freqs, lum, freq_band=None):
    if freq_band is None:
        Lbol = sci_integ.simps(freqs * lum, x=np.log(freqs))
    else:
        Lbol = 0.
        nu_min, nu_max = freq_band
        if nu_min < freqs[0]:
            print("nu_min =", nu_min, "\nminimum frequency in array =", freqs[0])
            return Lbol
        if nu_max > freqs[-1]:
            print("nu_max =", nu_max, "\nmaximum frequency in array=", freqs[-1])
            return Lbol
        if nu_max == nu_min:
            print("nu_max should not be equal to nu_min")
            return Lbol
        nu_mskd = ma.masked_outside(freqs, nu_min, nu_max)
        nus = nu_mskd.compressed()
        Lnu = lum[~nu_mskd.mask]
        Lbol = sci_integ.simps(nus * Lnu, x=np.log(nus))
    return Lbol


#  ###   ##                                    ##
#   #   #   #####          ####  #####   ####    #
#   #  #      #           #    # #    # #         #
#   #  #      #           #    # #####   ####     #
#   #  #      #           #    # #    #      #    #
#   #   #     #           #    # #    # #    #   #
#  ###   ##   #            ####  #####   ####  ##
#                 #######
def Itobs(t, nu, jnut, sen_lum, R, muc, Gbulk, muo, z, D):
    pwl = pwlf.PwlInteg()
    Itobs = np.zeros_like(jnut)
    i_edge = np.argmin(np.abs(2.0 * R * muc - sen_lum))
    if (sen_lum[i_edge] > 2.0 * R * muc):
        i_edge = i_edge - 1

    for j in range(nu.size):
        for i in range(t.size):

            if (i <= i_edge):
                i_start = 0
            else:
                i_start = i - i_edge

            for ii in range(i_start, i):
                tob_min = SR.t_com(t[i], z, Gbulk, muo, x=t[ii - 1] * C.cLight * muc)
                tob_max = SR.t_com(t[i], z, Gbulk, muo, x=t[ii] * C.cLight * muc)

                if ii == 0:
                    Itobs[i, j] = np.abs(tob_max - tob_min) * jnut[0, j]
                else:
                    if (jnut[ii, j] > 1e-100) & (jnut[ii - 1, j] > 1e-100):
                        sind = -np.log(jnut[ii, j] / jnut[ii - 1, j]) / np.log(tob_max / tob_min)
                        if (sind < -8.0):
                            sind = -8.0
                        if (sind > 8.0):
                            sind = 8.0
                        Itobs[i, j] = Itobs[i, j] + jnut[ii - 1, j] * tob_min * \
                            pwl.P(tob_max / tob_min, sind, 1e-6) / \
                            (Gbulk * muc * (muo - SR.bofg(Gbulk)) * D)
    return Itobs


#  #      #  ####  #    # #####  ####  #    # #####  #    # ######  ####
#  #      # #    # #    #   #   #    # #    # #    # #    # #      #
#  #      # #      ######   #   #      #    # #    # #    # #####   ####
#  #      # #  ### #    #   #   #      #    # #####  #    # #           #
#  #      # #    # #    #   #   #    # #    # #   #   #  #  #      #    #
#  ###### #  ####  #    #   #    ####   ####  #    #   ##   ######  ####
class LightCurves:
    def __init__(self):
        pass

    def nearest(self, nu_in, nus, flux):
        '''This function returns the light curve of the frequency nearest to
        the frequency given: nu_in.
        '''
        nu_pos, nu = misc.find_nearest(nus, nu_in)
        print("Nearest frequency: {0} Hz".format(misc.fortran_double(nu, dble=False)))
        return flux[:, nu_pos]

    def pwl_interp(self, nu_in, t, nus, flux):
        '''This function returns a power-law interpolated light curve
        '''
        nu_pos, nu = misc.find_nearest(nus, nu_in)
        lc = np.zeros_like(t)
        flux /= nus
        if len(nus) > 1:
            if nu > nu_in:
                nu_pos += 1
            if nus[nu_pos] >= nus[-1]:
                nu_pos -= 1
            for i in range(t.size):
                if (flux[i, nu_pos] > 1e-100) & (flux[i, nu_pos + 1] > 1e-100):
                    s = -np.log(flux[i, nu_pos + 1] / flux[i, nu_pos]) / \
                        np.log(nus[nu_pos + 1] / nus[nu_pos])
                    if s > 8.0:
                        s = 8.0
                    if s < -8.0:
                        s = -8.0
                    lc[i] = flux[i, nu_pos] * (nu_in / nus[nu_pos])**s
        else:
            lc = self.nearest(nu, nus, flux)
        return lc

    def integ(self, nu_min, nu_max, G, z, mu, freqs, flux):
        '''This function returns the integrated light curve in the given frequency band[nu_min, nu_max]
        '''
        licur = np.zeros_like(G)
        if nu_min < freqs[0]:
            print("nu_min =", nu_min, "\nminimum frequency in array =", freqs[0])
            return licur
        if nu_max > freqs[-1]:
            print("nu_max =", nu_max, "\nmaximum frequency in array=", freqs[-1])
            return licur

        if nu_max == nu_min:
            for i in range(G.size):
                licur[i] = np.exp(np.interp(np.log(nu_max), np.log(freqs),
                                            np.log(flux[i, :], where=(flux[i, :] != 0.0))))
        else:
            for i in range(G.size):
                nu_mskd = ma.masked_outside(SR.nu_obs(freqs, z, G[i], mu), nu_min, nu_max)
                nus = nu_mskd.compressed()
                Fnu = flux[i, ~nu_mskd.mask] / nus
                # NOTE: The integral is logarithmic, therfore the nus multiplying Fnu
                licur[i] = sci_integ.simps(nus * Fnu, x=np.log(nus))

        return licur

    def Fnu_lc(self, nuo1, nuo2, to, nuo, flux, tol=1e-6, dmax=15):
        licur = np.zeros_like(to)
        for i in range(to.size):
            iFnu = interpol.interp1d(nuo, flux[i, :] / nuo, kind='cubic', fill_value="extrapolate")
            if nuo1 == nuo2:
                licur[i] = iFnu(nuo1) * nuo1
            else:
                licur[i] = sci_integ.romberg(iFnu, nuo1, nuo2, tol=tol, divmax=dmax)
        return licur


#   ####  #####  ######  ####  ##### #####    ##
#  #      #    # #      #    #   #   #    #  #  #
#   ####  #    # #####  #        #   #    # #    #
#       # #####  #      #        #   #####  ######
#  #    # #      #      #    #   #   #   #  #    #
#   ####  #      ######  ####    #   #    # #    #
class spectrum:
    def __init__(self):
        pass

    def nearest(self, t_in, times, flux):
        '''This function returns the spectrum at th nearest time to
        the given one: t_in.
        '''
        t_pos, t = misc.find_nearest(times, t_in)
        print("Nearest time: {0} s".format(misc.sci_notation(t)))
        return flux[t_pos, :]

    def pwl_interp(self, t_in, nuo, times, flux):
        '''This function returns a power-law interpolated spectrum at the given time: t_in
        '''
        t_pos, t = misc.find_nearest(times, t_in)
        if t > t_in:
            t_pos += 1
        if times[t_pos] >= times[-1]:
            t_pos -= 1
        spec = np.zeros_like(nuo)
        for j in range(nuo.size):
            if (flux[t_pos, j] > 1e-100) & (flux[t_pos + 1, j] > 1e-100):
                s = -np.log(flux[t_pos + 1, j] / flux[t_pos, j]) / \
                    np.log(times[t_pos + 1] / times[t_pos])
                if s > 8.0:
                    s = 8.0
                if s < -8.0:
                    s = -8.0
                spec[j] = flux[t_pos, j] * (t_in / times[t_pos])**s
        return spec

    def spec_arr(self, to1, to2, to, nuo, flux):
        '''This function returns the integrated spectrum during the period[t_min, t_max]
         '''

        if to2 < to[0]:
            print("Input t_max: ", to2, "\nMinimum time in array:", to[0])
            return np.zeros_like(nuo)
        if to1 > to[-1]:
            print("Input t_min: ", to1, "\nMaximum time in array:", to[-1])
            return np.zeros_like(nuo)

        if to1 < to[0]:
            t_min = to[0]
            print("Input t_min: ", t_min, "\nMinimum time in array:", to[0])
        if to2 > to[-1]:
            t_max = to[-1]
            print("Input t_max: ", t_max, "\nMaximum time in array:", to[-1])

        if t_max == t_min:
            return self.pwl_interp(t_min, nuo, to, flux)

        spec = np.zeros_like(nuo)

        if (t_min == to[0]) & (t_max == to[-1]):
            tt = to
            Fnu = flux
        else:
            t_mskd = ma.masked_outside(to, t_min, t_max)
            tt = t_mskd.compressed()
            Fnu = flux[~t_mskd.mask, :]

        for j in range(nuo.size):
            spec[j] = sci_integ.simps(tt * Fnu[:, j], x=np.log(tt))
            # spec[j] = sci_integ.simps(Fnu[:, j], x=tt)

        return spec / (tt[-1] - tt[0])

    def spec_interp(self, to1, to2, to, nuo, flux, tol=1e-6, dmax=15):
        spec = np.zeros_like(nuo)
        for j in range(nuo.size):
            iFnu = interpol.interp1d(to, flux[:, j], kind='cubic', fill_value="extrapolate")
            spec[j] = sci_integ.romberg(iFnu, to1, to2, tol=tol, divmax=dmax)
        return spec / (to2 - to1)

#   #####                                           ######
#  #     #  ####  #    # #####  #####  ####  #    # #     #  ####  #    #
#  #       #    # ##  ## #    #   #   #    # ##   # #     # #    # ##  ##
#  #       #    # # ## # #    #   #   #    # # #  # #     # #    # # ## #
#  #       #    # #    # #####    #   #    # #  # # #     # #    # #    #
#  #     # #    # #    # #        #   #    # #   ## #     # #    # #    #
#   #####   ####  #    # #        #    ####  #    # ######   ####  #    #
def ComptonDom(nus, Fsyn, Fic, t_min, t_max, times):
    spec = spectrum()
    # pwli = pwlf.PwlInteg()
    # Nf = nus.size

    # NOTE  synchrotron spectrum and peak
    synint = spec.integ(t_min, t_max, nus, times, Fsyn)
    syn_pos = synint.argmax()
    syn_peak = synint[syn_pos]
    nu_syn = nus[syn_pos]

    # syntot = 0.0
    # for j in range(Nf - 1):
    #     if (synint[j] > 1e-100) & (synint[j + 1] > 1e-100):
    #         s = -np.log(synint[j + 1] / synint[j]) / np.log(nus[j + 1] / nus[j])
    #         syntot += synint[j] * nus[j] * pwli.P(nus[j + 1] / nus[j], s)

    # NOTE  IC spectrum and peak
    ICint = spec.integ(t_min, t_max, nus, times, Fic)
    IC_pos = ICint.argmax()
    IC_peak = ICint[IC_pos]
    nu_IC = nus[IC_pos]

    # ICtot = 0.0
    # for j in range(Nf - 1):
    #     if (ICint[j] > 1e-100) & (ICint[j + 1] > 1e-100):
    #         s = -np.log(ICint[j + 1] / ICint[j]) / np.log(nus[j + 1] / nus[j])
    #         ICtot += ICint[j] * nus[j] * pwli.P(nus[j + 1] / nus[j], s)

    A_C = IC_peak / syn_peak
    return nu_syn, nu_IC, A_C


#  ######                                       #######
#  #     # #    #  ####  #####  ####  #    #    #       #      #    # #    #
#  #     # #    # #    #   #   #    # ##   #    #       #      #    #  #  #
#  ######  ###### #    #   #   #    # # #  #    #####   #      #    #   ##
#  #       #    # #    #   #   #    # #  # #    #       #      #    #   ##
#  #       #    # #    #   #   #    # #   ##    #       #      #    #  #  #
#  #       #    #  ####    #    ####  #    #    #       ######  ####  #    #
def Fph(nu_min, nu_max, freqs, Fnu):
    '''Calculate the photon flux for the frequency band [nu_min, nu_max] from
    a given flux density.

    Input:
        nu_min, nu_max: scalars
        freqs: array
        Fnu: array

    Output:
        Photon flux: scalar
        photon flux spectral indices: array
    '''

    if nu_min < freqs[0] or nu_max > freqs[-1]:
        return print('Error: nu_min and nu_max outside frequencies array')

    nu_mskd = ma.masked_outside(freqs, nu_min, nu_max)
    nus = nu_mskd.compressed()
    flux = Fnu[~nu_mskd.mask] / nus
    num_nus = len(nus)

    integral = 0.0
    pwli = pwlf.PwlInteg()
    for i in range(num_nus - 1):
        if (flux[i] > 1e-100) & (flux[i + 1] > 1e-100):
            s = -np.log(flux[i + 1] / flux[i]) / np.log(nus[i + 1] / nus[i])
            integral += flux[i] * nus[i] * pwli.P(nus[i + 1] / nus[i], s)
    return nus[:-1], flux[:-1], integral / C.hPlanck


#  #####  #    #  ####  #####  ####  #    #    # #    # #####
#  #    # #    # #    #   #   #    # ##   #    # ##   # #    #
#  #    # ###### #    #   #   #    # # #  #    # # #  # #    #
#  #####  #    # #    #   #   #    # #  # #    # #  # # #    #
#  #      #    # #    #   #   #    # #   ##    # #   ## #    #
#  #      #    #  ####    #    ####  #    #    # #    # #####
def photon_index(freqs, flux):
    def line(x, a, b):
        return a * x + b
    lnu = np.log10(freqs)
    lfl = np.log10(np.where(1e-100 > flux, 1e-100, flux))
    popt, pcov = op.curve_fit(line, lnu, lfl)
    return np.power(10.0, line(np.log10(freqs), *popt)), popt, pcov
